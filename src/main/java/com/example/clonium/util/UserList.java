package com.example.clonium.util;

import com.example.clonium.db.User;

import java.util.ArrayList;
import java.util.List;

public class UserList {
    private static final List<User> users = new ArrayList<>();

    private UserList() {

    }

    public static List<User> userList() {

        User user1 = new User();
        user1.setId(1L);
        user1.setName("David");

        User user2 = new User();
        user2.setId(2L);
        user2.setName("Tiko");

        User user3 = new User();
        user3.setId(3L);
        user3.setName("Tiran");

        User user4 = new User();
        user4.setId(4L);
        user4.setName("Vahag");

        User user5 = new User();
        user5.setId(5L);
        user5.setName("Isso");

        User user6 = new User();
        user6.setId(6L);
        user6.setName("Gogor");

        User user7 = new User();
        user7.setId(7L);
        user7.setName("Vladimir");

        User user8 = new User();
        user8.setId(8L);
        user8.setName("Artyom");

        User user9 = new User();
        user9.setId(9L);
        user9.setName("Aleksey");

        User user10 = new User();
        user10.setId(10L);
        user10.setName("Arkadi");

        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);
        users.add(user10);

        return users;
    }

    public static User getUser(long userId) {
        return UserList.userList().stream().filter(t -> t.getId().equals(userId)).findFirst().orElse(new User(100L, "Ankap user"));
    }

}
