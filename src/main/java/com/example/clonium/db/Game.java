package com.example.clonium.db;

import com.example.clonium.util.UserList;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Game {

    private final Point[][] points;

    @Getter
    private final List<Player> players = new ArrayList<>();

    @Getter
    private int currentPlayerIndex = 0;

    private int colorIndex = 0;

    @Getter
    private final int maxCount;

    @Getter
    private final Long id;

    public Game(Long id, int maxCount, Point[][] points) {
        this.id = id;
        this.maxCount = maxCount;
        this.points = points;
        for (int y = 0; y < points.length; y++) {
            for (int x = 0; x < points.length; x++) {
                points[y][x] = new Point(x, y);
            }
        }
    }

    public void startGame() {
        if (!isMaxCount()) {
            throw new IllegalStateException();
        }

        if (players.size() == 2) {
            Point playerPoint1 = points[0][0];
            playerPoint1.setColor(players.get(0).getColor());
            playerPoint1.setCount(3);

            Point playerPoint2 = points[0][1];
            playerPoint2.setColor(players.get(1).getColor());
            playerPoint2.setCount(3);
        }

        if (players.size() == 3) {
            Point playerPoint1 = points[0][0];
            playerPoint1.setColor(players.get(0).getColor());
            playerPoint1.setCount(3);

            Point playerPoint2 = points[0][1];
            playerPoint2.setColor(players.get(1).getColor());
            playerPoint2.setCount(3);

            Point playerPoint3 = points[points.length - 1][0];
            playerPoint3.setColor(players.get(2).getColor());
            playerPoint3.setCount(3);
        }

        if (players.size() == 4) {
            Point playerPoint1 = points[0][0];
            playerPoint1.setColor(players.get(0).getColor());
            playerPoint1.setCount(3);

            Point playerPoint2 = points[0][points.length - 1];
            playerPoint2.setColor(players.get(1).getColor());
            playerPoint2.setCount(3);

            Point playerPoint3 = points[points.length - 1][0];
            playerPoint3.setColor(players.get(2).getColor());
            playerPoint3.setCount(3);

            Point playerPoint4 = points[points.length - 1][points.length - 1];
            playerPoint4.setColor(players.get(3).getColor());
            playerPoint4.setCount(3);
        }

    }

    public void addPlayerId(long id) {
        Player player = new Player(new User(id, UserList.getUser(id).getName()), Color.values()[colorIndex++]);
        players.add(player);
    }

    public void draw() {
        for (Point[] point : points) {
            for (int x = 0; x < points.length; x++) {
                System.out.print((point[x]).getCount() + " ");
            }
            System.out.println();
        }
    }

    public Point[][] getPoints() {
        return points;
    }

    public void turn(int i, int j) {
        Point point = points[i][j];

        if (point.getCount() == 0) {
            throw new IllegalArgumentException("Empty point");
        }
        if (players.get(currentPlayerIndex).getColor() != point.getColor()) {
            throw new IllegalArgumentException("not your turn");
        }

        turn(i, j, point.getColor());
        currentPlayerIndex = incrementCurrentPlayerIndex(currentPlayerIndex);
    }

    private void turn(int i, int j, Color color) {
        Point point = points[i][j];

        point.increment();
        point.setColor(color);

        if (point.getCount() == 4) {
            point.zero();
            for (Point neighbor : findNeighbors(i, j)) {
                turn(neighbor.getI(), neighbor.getJ(), color);
            }
        }
    }

    private List<Point> findNeighbors(int i, int j) {
        List<Point> neighbors = new ArrayList<>(4);
        if (i - 1 >= 0) {
            neighbors.add(new Point(i - 1, j));
        }
        if (i + 1 < points.length) {
            neighbors.add(new Point(i + 1, j));
        }
        if (j - 1 >= 0) {
            neighbors.add(new Point(i, j - 1));
        }
        if (j + 1 < points[0].length) {
            neighbors.add(new Point(i, j + 1));
        }
        return neighbors;
    }

    public boolean isMaxCount() {
        return maxCount == players.size();
    }

    public int getConnectedUsersCount() {
        return players.size();
    }

    public boolean isMyTurn(Long userId) {
        return players.get(currentPlayerIndex).getUser().getId().equals(userId);
    }

    public List<Long> loseList(Long userId) {
        List<Player> loseList = new ArrayList<>();
        int a = 0;
        for (Player player : getPlayers()) {
            if (!player.getUser().getId().equals(userId)) {
                for (Point[] point : points) {
                    for (Point value : point) {
                        if (player.getColor().equals(value.getColor())) {
                            a++;
                        }
                    }
                }
                if (a == 0) {
                    loseList.add(player);
                } else {
                    a = 0;
                }
            }

        }
        return loseList.stream().map(Player::getUser).map(User::getId).collect(Collectors.toList());
    }

    public void incrementLosePlayersIndex(List<Integer> losePlayersIndex) {
        for (Integer playersIndex : losePlayersIndex) {
            if (playersIndex.equals(currentPlayerIndex)) {
                currentPlayerIndex = incrementCurrentPlayerIndex(currentPlayerIndex);
            }
        }
    }

    private int incrementCurrentPlayerIndex(int currentPlayerIndex) {
        if (currentPlayerIndex != players.size() - 1) {
            currentPlayerIndex++;
        } else {
            currentPlayerIndex = 0;
        }
        return currentPlayerIndex;
    }

}
