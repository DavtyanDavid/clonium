package com.example.clonium.db;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Player {

    private final User user;

    private final Color color;

}
