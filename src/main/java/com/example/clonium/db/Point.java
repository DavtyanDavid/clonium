package com.example.clonium.db;

import lombok.Getter;

@Getter
public class Point {

    private final int i;
    private final int j;
    private int count = 0;
    private Color color;

    public Point(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public void increment() {
        this.count++;
    }

    public void zero() {
        this.count = 0;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
