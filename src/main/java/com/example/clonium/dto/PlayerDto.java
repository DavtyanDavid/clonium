package com.example.clonium.dto;

import com.example.clonium.db.Color;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PlayerDto {

    private final UserDto user;

    private final Color color;

}
