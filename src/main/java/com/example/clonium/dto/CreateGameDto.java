package com.example.clonium.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateGameDto {

    private int maxCount;
    private int length;
    private int height;

}
