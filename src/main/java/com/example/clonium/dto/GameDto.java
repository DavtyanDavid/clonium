package com.example.clonium.dto;

import lombok.Getter;

@Getter
public class GameDto {

    private final long id;

    private final int count;

    private final int maxCount;

    private final int length;

    private final int height;

    public GameDto(long id, int count, int maxCount, int length, int height) {
        this.id = id;
        this.count = count;
        this.maxCount = maxCount;
        this.length = length;
        this.height = height;
    }
}
