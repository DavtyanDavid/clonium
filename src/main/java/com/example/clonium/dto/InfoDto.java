package com.example.clonium.dto;

import com.example.clonium.db.Point;
import lombok.Getter;

import java.util.List;

@Getter
public class InfoDto {

    private final List<PlayerDto> players;

    private final Point[][] points;

    public InfoDto(List<PlayerDto> players, Point[][] points) {
        this.players = players;
        this.points = points;
    }
}
