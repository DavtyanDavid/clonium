package com.example.clonium.controlors;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

public class UjexWebSocketHandler implements WebSocketHandler {

    private final WebSocketHandler webSocketHandler;

    public UjexWebSocketHandler(WebSocketHandler webSocketHandler) {
        this.webSocketHandler = webSocketHandler;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        try {
            webSocketHandler.afterConnectionEstablished(session);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        try {
            webSocketHandler.handleMessage(session, message);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        try {
            webSocketHandler.handleTransportError(session, exception);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        try {
            webSocketHandler.afterConnectionClosed(session, closeStatus);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean supportsPartialMessages() {
        return webSocketHandler.supportsPartialMessages();
    }
}
