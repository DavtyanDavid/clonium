package com.example.clonium.controlors;

public class WebSocketData {

    private final String type;
    private final Object content;

    public WebSocketData(String type, Object content) {
        this.type = type;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public Object getContent() {
        return content;
    }
}
