package com.example.clonium.controlors;

import com.example.clonium.db.*;
import com.example.clonium.dto.*;
import com.example.clonium.util.UserList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class MyWebSocketHandler extends AbstractWebSocketHandler {

    private final Map<WebSocketSession, Long> userSessions = new HashMap<>();

    private final Map<Long, WebSocketSession> sessionsUser = new HashMap<>();

    private final Map<Long, Game> games = new HashMap<>();

    private Long userIdGenerator = 1L;

    private Long gameIdGenerator = 1L;

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws IOException {
        Long userId = userSessions.get(session);
        String payload = message.getPayload();
        ObjectMapper objectMapper = new ObjectMapper();

        ObjectNode objectNode = (ObjectNode) objectMapper.readTree(payload);
        String type = objectNode.get("type").asText();

        switch (type) {
            case "get-game-list":
                sendToAll(new WebSocketData("game-list", gamesToDto(games.values())));
                break;
            case "create-new-game":
                CreateGameDto dto = objectMapper.treeToValue(objectNode.get("data"), CreateGameDto.class);
                Game game = new Game(gameIdGenerator, dto.getMaxCount(), new Point[dto.getLength()][dto.getHeight()]);
                games.put(gameIdGenerator, game);
                gameIdGenerator++;

                sendToAll(new WebSocketData("game-list", gamesToDto(games.values())));
                break;
            case "turn":
                TurnDto turn = objectMapper.treeToValue(objectNode.get("data"), TurnDto.class);
                Game currentGame = games.get(turn.getGameId());

                if (currentGame.isMyTurn(userId)) {

                    currentGame.turn(turn.getI(), turn.getJ());

                    List<Long> losePlayersId = currentGame.loseList(userId);
                    List<Player> currentGamePlayers = currentGame.getPlayers();

                    List<Integer> losePlayersIndex = new ArrayList<>();

                    for (int i = 0; i < currentGamePlayers.size(); i++) {
                        for (Long losePlayerId : losePlayersId) {
                            if (currentGamePlayers.get(i).getUser().getId().equals(losePlayerId)) {
                                losePlayersIndex.add(i);
                            }
                        }
                    }

                    currentGame.incrementLosePlayersIndex(losePlayersIndex);

                    for (Player player : currentGame.getPlayers()) {
                        sendMessage(new WebSocketData("game-matrix", currentGame.getPoints()), sessionsUser.get(player.getUser().getId()));
                    }
                }
                break;
            case "join-game":
                JoinGameDto joinGameDto = objectMapper.treeToValue(objectNode.get("data"), JoinGameDto.class);
                Game joinGame = games.get(joinGameDto.getGameId());
                joinGame.addPlayerId(userId);

                if (joinGame.isMaxCount()) {
                    joinGame.startGame();
                    sendToAll(new WebSocketData("start-game", new InfoDto(playersToDto(joinGame.getPlayers()), joinGame.getPoints())));
                }
                break;

            default:
                throw new IllegalStateException();
        }
    }

    public void sendToAll(WebSocketData data) {
        for (WebSocketSession webSocketSession : userSessions.keySet()) {
            try {
                sendMessage(data, webSocketSession);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(WebSocketData data, WebSocketSession webSocketHandler) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String carAsString = objectMapper.writeValueAsString(data);
        webSocketHandler.sendMessage(new TextMessage(carAsString));
    }

    private List<GameDto> gamesToDto(Collection<Game> games) {
        return games.stream()
                .map(game -> new GameDto(game.getId(), game.getConnectedUsersCount(), game.getMaxCount(),game.getPoints().length,game.getPoints().length))
                .collect(Collectors.toList());
    }

    private List<PlayerDto> playersToDto(Collection<Player> players) {
        return players.stream().map(player -> userDtoToPlayerDto(userToDto(player.getUser()), player.getColor())).collect(Collectors.toList());
    }

    private UserDto userToDto(User user) {
        return new UserDto(user.getId(), user.getName());
    }

    private PlayerDto userDtoToPlayerDto(UserDto userDto, Color color) {
        return new PlayerDto(userDto, color);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        User user = UserList.getUser(userIdGenerator);
        sendMessage(new WebSocketData("user", userToDto(user)), session);

        userSessions.put(session, user.getId());
        sessionsUser.put(user.getId(), session);
        userIdGenerator++;
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        Long userId = userSessions.remove(session);
        sessionsUser.remove(userId);
    }
}
